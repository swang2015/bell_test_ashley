# Toxic comment classification

- `notebooks` contains notebook for training the model
- `API` contains source files to serve the model

## Serving the model

* POST <http://localhost:8090/api/model/{model_name}> will return a prediction using the ML model.
  The JSON data with list of comments must be supplied. The service will validate that all the mandatory values are passed. Return values are:
  * 500 - "Internal Server Error"
  * 400 - Validation error if any mandatory parameter is missing or if any wrong data type is supplied
  * 200 - Predicted value based on JSON input

These two methods are used for the liveness probes in a Kubernetes deployment:

* GET <http://localhost:8888/api/liveness> returns 200/"Alive" if the service is up and running


## Build automation

This project is built into a Docker image using the Docker Hub automated build at <https://hub.docker.com/r/swang2048/bell_test/>


## Running the Docker container

```
docker run -d -p 8090:8090 swang2048/bell_test:v1
```

## Tool stack

- Docker
- gitlab cicd
- pytest
- Jupyter notebook
- sklearn
- Flask & uwsgi
- swagger
- (GAE)
