import main
import pytest


# def test_health(client):
#     response = client.get('/health')
#     assert response.status_code == 200

def test_index():
    main.app.testing = True
    client = main.app.app.test_client()

    r = client.get('/')
    assert r.status_code == 302
