"""
Api to select and serve sentence classification models
Created by Ashley Wang, 2021 Jan
"""

import os
import connexion
import logging.config
from logging import Logger, getLogger

from flask_cors import CORS
from flask import Flask, redirect


IN_UWSGI: bool = True
try:
    import uwsgi
except ImportError:
    IN_UWSGI = False


app = connexion.App(__name__)
app.add_api('swagger.yml')
CORS(app.app)

logging.config.fileConfig(
    os.path.normpath(os.path.join(os.path.dirname(__file__), "../logging.conf"))
)
log: Logger = getLogger(__name__)


@app.route('/')
def swagger():
    return redirect('/api/ui')


def main():
    """Main routine, executed only if running as stand-alone."""
    log.info("***** Starting development server *****")
    app.run(
        debug=True,
        port='8090',
        host='0.0.0.0',
    )


if __name__ == '__main__':
    main()
