"""This module implements the TrainedModelWrapper class."""

# Author: Ashley Wang

import pandas as pd
from abc import ABC, abstractmethod
from typing import Iterable, Dict


class TrainedModelWrapper(ABC):
    """
    TrainedModelWrapper acts as the template to serve any trained model.
    """
    def __init__(self):
        self.model = None
        self.pred_cols = []
        self._load()
    
    @staticmethod
    @abstractmethod
    def _load(self):
        """Loads a trained model"""
        pass
    
    @abstractmethod
    def _run(self, data: pd.DataFrame) -> Iterable:
        """Model inference"""
        pass
    
    @abstractmethod
    def _format_output(self, data) -> Dict:
        """Model inference"""
        pass
    
    def _format_input(self, data: Iterable) -> pd.DataFrame:
        """json data to pd dataframe"""
        return pd.DataFrame.from_dict(data)

    def run(self, data: Iterable) -> Iterable:
        """Calls the wrapped run() method if it's assigned."""
        df = self._format_input(data)
        res = self._run(df)
        return self._format_output(res)
    