"""This module contains models trained with Scikit-Learn."""

# Author: Ashley Wang


import numpy as np, pandas as pd
import joblib
import re, string
from .wrapper import TrainedModelWrapper
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer


class NBSVM(TrainedModelWrapper):
    """Naive Bayes Support Vector Machine"""
    def __init__(self):
        super().__init__()
        self.pred_cols = ['toxic', 'severe_toxic', 'obscene', 'threat', 'insult', 'identity_hate']

    def _load(self):
        self.models = joblib.load('model_files/nb_svm_models.pkl')
        self.vec = joblib.load('model_files/nb_svm_vectorizer.pkl')
    
    def _run(self, data):
        re_tok = re.compile(f'([{string.punctuation}“”¨«»®´·º½¾¿¡§£₤‘’])')
        def tokenize(s): return re_tok.sub(r' \1 ', s).split()

        term_doc = self.vec.transform(data['comments'])
        
        preds = np.zeros((len(data), len(self.pred_cols)))

        for i, svm in enumerate(self.models):
            model, weight = svm
            preds[:,i] = model.predict_proba(term_doc.multiply(weight))[:,1]
        
        pred_df = pd.DataFrame(preds, columns=self.pred_cols)

        pred_df[pred_df>=0.5] = 1
        pred_df[pred_df<0.5] = 0
        return pred_df
    
    def _format_output(self, output):
        return output.to_dict('records')
