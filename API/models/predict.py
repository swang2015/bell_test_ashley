"""This module performs as factory to the trained model class"""

# Author: Ashley Wang

from .sklearn import NBSVM

def factory(model_name, data):
    if model_name == 'nb_svm':
        return NBSVM().run(data)
    else:
        return {'msg': 'model not found!'}, 404